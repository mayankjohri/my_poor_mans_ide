import wx

# import the newly created GUI file
from ui_frame.main_frame import MainFrame


class MainWindow(MainFrame):
    def __init__(self, parent):
        super().__init__(parent)


if __name__ == "__main__":
    app = wx.App(False)
    frame = MainWindow(None)
    frame.Show(True)
    # start the applications
    app.MainLoop()
