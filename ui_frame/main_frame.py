# -*- coding: utf-8 -*-

###########################################################################
## Python code generated with wxFormBuilder (version 3.9.0 Mar  1 2021)
## http://www.wxformbuilder.org/
##
## PLEASE DO *NOT* EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc
import wx.aui
import wx.stc

###########################################################################
## Class MainFrame
###########################################################################

class MainFrame ( wx.Frame ):

	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"MyPoorMansIDE", pos = wx.DefaultPosition, size = wx.Size( 655,385 ), style = wx.CAPTION|wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )

		self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )

		bSizer1 = wx.BoxSizer( wx.VERTICAL )

		self.m_splitter1 = wx.SplitterWindow( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.SP_3D|wx.SP_3DBORDER )
		self.m_splitter1.Bind( wx.EVT_IDLE, self.m_splitter1OnIdle )

		self.m_panel1 = wx.Panel( self.m_splitter1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer3 = wx.BoxSizer( wx.VERTICAL )

		bSizer6 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_auiToolBar1 = wx.aui.AuiToolBar( self.m_panel1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.aui.AUI_TB_HORZ_LAYOUT )
		self.m_topen = self.m_auiToolBar1.AddTool( wx.ID_ANY, u"Open Folder", wx.NullBitmap, wx.NullBitmap, wx.ITEM_NORMAL, wx.EmptyString, wx.EmptyString, None )

		self.m_tnew = self.m_auiToolBar1.AddTool( wx.ID_ANY, u"New", wx.NullBitmap, wx.NullBitmap, wx.ITEM_NORMAL, wx.EmptyString, wx.EmptyString, None )

		self.m_auiToolBar1.AddSeparator()

		self.m_tup = self.m_auiToolBar1.AddTool( wx.ID_ANY, u"Up", wx.NullBitmap, wx.NullBitmap, wx.ITEM_NORMAL, wx.EmptyString, wx.EmptyString, None )

		self.m_auiToolBar1.Realize()

		bSizer6.Add( self.m_auiToolBar1, 0, wx.ALL, 5 )


		bSizer3.Add( bSizer6, 0, wx.EXPAND, 5 )

		self.m_gdFileList = wx.GenericDirCtrl( self.m_panel1, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.DIRCTRL_3D_INTERNAL|wx.SUNKEN_BORDER, u"*.py;*.c;*.rb;*.rs", 0 )

		self.m_gdFileList.ShowHidden( False )
		bSizer3.Add( self.m_gdFileList, 1, wx.EXPAND |wx.ALL, 5 )


		self.m_panel1.SetSizer( bSizer3 )
		self.m_panel1.Layout()
		bSizer3.Fit( self.m_panel1 )
		self.m_panel2 = wx.Panel( self.m_splitter1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer2 = wx.BoxSizer( wx.VERTICAL )

		self.m_auinotebook1 = wx.aui.AuiNotebook( self.m_panel2, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.aui.AUI_NB_CLOSE_BUTTON|wx.aui.AUI_NB_DEFAULT_STYLE|wx.aui.AUI_NB_MIDDLE_CLICK_CLOSE|wx.aui.AUI_NB_TAB_MOVE )
		self.m_panel3 = wx.Panel( self.m_auinotebook1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer5 = wx.BoxSizer( wx.VERTICAL )

		self.m_sc_code = wx.stc.StyledTextCtrl( self.m_panel3, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, 0)
		self.m_sc_code.SetUseTabs ( True )
		self.m_sc_code.SetTabWidth ( 4 )
		self.m_sc_code.SetIndent ( 4 )
		self.m_sc_code.SetTabIndents( True )
		self.m_sc_code.SetBackSpaceUnIndents( True )
		self.m_sc_code.SetViewEOL( False )
		self.m_sc_code.SetViewWhiteSpace( False )
		self.m_sc_code.SetMarginWidth( 2, 0 )
		self.m_sc_code.SetIndentationGuides( True )
		self.m_sc_code.SetReadOnly( False );
		self.m_sc_code.SetMarginType ( 1, wx.stc.STC_MARGIN_SYMBOL )
		self.m_sc_code.SetMarginMask ( 1, wx.stc.STC_MASK_FOLDERS )
		self.m_sc_code.SetMarginWidth ( 1, 16)
		self.m_sc_code.SetMarginSensitive( 1, True )
		self.m_sc_code.SetProperty ( "fold", "1" )
		self.m_sc_code.SetFoldFlags ( wx.stc.STC_FOLDFLAG_LINEBEFORE_CONTRACTED | wx.stc.STC_FOLDFLAG_LINEAFTER_CONTRACTED );
		self.m_sc_code.SetMarginType( 0, wx.stc.STC_MARGIN_NUMBER );
		self.m_sc_code.SetMarginWidth( 0, self.m_sc_code.TextWidth( wx.stc.STC_STYLE_LINENUMBER, "_99999" ) )
		self.m_sc_code.MarkerDefine( wx.stc.STC_MARKNUM_FOLDER, wx.stc.STC_MARK_BOXPLUS )
		self.m_sc_code.MarkerSetBackground( wx.stc.STC_MARKNUM_FOLDER, wx.BLACK)
		self.m_sc_code.MarkerSetForeground( wx.stc.STC_MARKNUM_FOLDER, wx.WHITE)
		self.m_sc_code.MarkerDefine( wx.stc.STC_MARKNUM_FOLDEROPEN, wx.stc.STC_MARK_BOXMINUS )
		self.m_sc_code.MarkerSetBackground( wx.stc.STC_MARKNUM_FOLDEROPEN, wx.BLACK )
		self.m_sc_code.MarkerSetForeground( wx.stc.STC_MARKNUM_FOLDEROPEN, wx.WHITE )
		self.m_sc_code.MarkerDefine( wx.stc.STC_MARKNUM_FOLDERSUB, wx.stc.STC_MARK_EMPTY )
		self.m_sc_code.MarkerDefine( wx.stc.STC_MARKNUM_FOLDEREND, wx.stc.STC_MARK_BOXPLUS )
		self.m_sc_code.MarkerSetBackground( wx.stc.STC_MARKNUM_FOLDEREND, wx.BLACK )
		self.m_sc_code.MarkerSetForeground( wx.stc.STC_MARKNUM_FOLDEREND, wx.WHITE )
		self.m_sc_code.MarkerDefine( wx.stc.STC_MARKNUM_FOLDEROPENMID, wx.stc.STC_MARK_BOXMINUS )
		self.m_sc_code.MarkerSetBackground( wx.stc.STC_MARKNUM_FOLDEROPENMID, wx.BLACK)
		self.m_sc_code.MarkerSetForeground( wx.stc.STC_MARKNUM_FOLDEROPENMID, wx.WHITE)
		self.m_sc_code.MarkerDefine( wx.stc.STC_MARKNUM_FOLDERMIDTAIL, wx.stc.STC_MARK_EMPTY )
		self.m_sc_code.MarkerDefine( wx.stc.STC_MARKNUM_FOLDERTAIL, wx.stc.STC_MARK_EMPTY )
		self.m_sc_code.SetSelBackground( True, wx.SystemSettings.GetColour(wx.SYS_COLOUR_HIGHLIGHT ) )
		self.m_sc_code.SetSelForeground( True, wx.SystemSettings.GetColour(wx.SYS_COLOUR_HIGHLIGHTTEXT ) )
		bSizer5.Add( self.m_sc_code, 1, wx.EXPAND |wx.ALL, 5 )


		self.m_panel3.SetSizer( bSizer5 )
		self.m_panel3.Layout()
		bSizer5.Fit( self.m_panel3 )
		self.m_auinotebook1.AddPage( self.m_panel3, u"a page", False, wx.NullBitmap )

		bSizer2.Add( self.m_auinotebook1, 1, wx.EXPAND |wx.ALL, 5 )

		bSizer4 = wx.BoxSizer( wx.VERTICAL )


		bSizer2.Add( bSizer4, 1, wx.EXPAND, 5 )


		self.m_panel2.SetSizer( bSizer2 )
		self.m_panel2.Layout()
		bSizer2.Fit( self.m_panel2 )
		self.m_splitter1.SplitVertically( self.m_panel1, self.m_panel2, 200 )
		bSizer1.Add( self.m_splitter1, 1, wx.EXPAND, 5 )


		self.SetSizer( bSizer1 )
		self.Layout()

		self.Centre( wx.BOTH )

		# Connect Events
		self.m_gdFileList.Bind( wx.EVT_DIRCTRL_FILEACTIVATED, self.open_file )

	def __del__( self ):
		pass


	# Virtual event handlers, overide them in your derived class
	def open_file( self, event ):
		event.Skip()

	def m_splitter1OnIdle( self, event ):
		self.m_splitter1.SetSashPosition( 200 )
		self.m_splitter1.Unbind( wx.EVT_IDLE )


